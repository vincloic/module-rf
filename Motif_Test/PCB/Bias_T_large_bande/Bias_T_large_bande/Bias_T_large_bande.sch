EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C1
U 1 1 5FB3D08F
P 3600 3400
F 0 "C1" V 3348 3400 50  0000 C CNN
F 1 "C" V 3439 3400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3638 3250 50  0001 C CNN
F 3 "~" H 3600 3400 50  0001 C CNN
	1    3600 3400
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5FB3D3B6
P 3600 3700
F 0 "C2" V 3348 3700 50  0000 C CNN
F 1 "C" V 3439 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3638 3550 50  0001 C CNN
F 3 "~" H 3600 3700 50  0001 C CNN
	1    3600 3700
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5FB3D7F4
P 5000 2500
F 0 "C4" V 4748 2500 50  0000 C CNN
F 1 "C" V 4839 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5038 2350 50  0001 C CNN
F 3 "~" H 5000 2500 50  0001 C CNN
	1    5000 2500
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5FB3DCC2
P 5000 2950
F 0 "C5" V 4748 2950 50  0000 C CNN
F 1 "C" V 4839 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5038 2800 50  0001 C CNN
F 3 "~" H 5000 2950 50  0001 C CNN
	1    5000 2950
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5FB3D5DA
P 5000 1950
F 0 "C3" V 4748 1950 50  0000 C CNN
F 1 "C" V 4839 1950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5038 1800 50  0001 C CNN
F 3 "~" H 5000 1950 50  0001 C CNN
	1    5000 1950
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5FB415FE
P 2900 3550
F 0 "J1" H 2828 3788 50  0000 C CNN
F 1 "Conn_Coaxial" H 2828 3697 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 2900 3550 50  0001 C CNN
F 3 " ~" H 2900 3550 50  0001 C CNN
	1    2900 3550
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J3
U 1 1 5FB41CE4
P 6000 3550
F 0 "J3" H 6100 3525 50  0000 L CNN
F 1 "Conn_Coaxial" H 6100 3434 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 6000 3550 50  0001 C CNN
F 3 " ~" H 6000 3550 50  0001 C CNN
	1    6000 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:L L3
U 1 1 5FB43B9F
P 4650 3150
F 0 "L3" H 4703 3196 50  0000 L CNN
F 1 "L" H 4703 3105 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4650 3150 50  0001 C CNN
F 3 "~" H 4650 3150 50  0001 C CNN
	1    4650 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 5FB44082
P 4650 2700
F 0 "L2" H 4703 2746 50  0000 L CNN
F 1 "L" H 4703 2655 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4650 2700 50  0001 C CNN
F 3 "~" H 4650 2700 50  0001 C CNN
	1    4650 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5FB443F1
P 4650 2200
F 0 "L1" H 4703 2246 50  0000 L CNN
F 1 "L" H 4703 2155 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4650 2200 50  0001 C CNN
F 3 "~" H 4650 2200 50  0001 C CNN
	1    4650 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5FB47498
P 4650 1350
F 0 "J2" V 4887 1279 50  0000 C CNN
F 1 "Conn_Coaxial" V 4796 1279 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 4650 1350 50  0001 C CNN
F 3 " ~" H 4650 1350 50  0001 C CNN
	1    4650 1350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FB47FAC
P 2900 3750
F 0 "#PWR01" H 2900 3500 50  0001 C CNN
F 1 "GND" H 2905 3577 50  0000 C CNN
F 2 "" H 2900 3750 50  0001 C CNN
F 3 "" H 2900 3750 50  0001 C CNN
	1    2900 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FB48413
P 6000 3750
F 0 "#PWR06" H 6000 3500 50  0001 C CNN
F 1 "GND" H 6005 3577 50  0000 C CNN
F 2 "" H 6000 3750 50  0001 C CNN
F 3 "" H 6000 3750 50  0001 C CNN
	1    6000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FB48976
P 4850 1350
F 0 "#PWR02" H 4850 1100 50  0001 C CNN
F 1 "GND" V 4855 1222 50  0000 R CNN
F 2 "" H 4850 1350 50  0001 C CNN
F 3 "" H 4850 1350 50  0001 C CNN
	1    4850 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3100 3550 3450 3550
Wire Wire Line
	3450 3550 3450 3400
Wire Wire Line
	3450 3550 3450 3700
Connection ~ 3450 3550
Wire Wire Line
	3750 3400 3750 3550
Wire Wire Line
	3750 3550 4650 3550
Connection ~ 3750 3550
Wire Wire Line
	3750 3550 3750 3700
Wire Wire Line
	4650 3550 4650 3300
Connection ~ 4650 3550
Wire Wire Line
	4650 3550 5800 3550
Wire Wire Line
	4650 3000 4650 2950
Wire Wire Line
	4850 2950 4650 2950
Connection ~ 4650 2950
Wire Wire Line
	4650 2950 4650 2850
Wire Wire Line
	4650 2350 4650 2500
Wire Wire Line
	4850 2500 4650 2500
Connection ~ 4650 2500
Wire Wire Line
	4650 2500 4650 2550
Wire Wire Line
	4850 1950 4650 1950
Wire Wire Line
	4650 1950 4650 2050
Wire Wire Line
	4650 1550 4650 1950
Connection ~ 4650 1950
$Comp
L power:GND #PWR03
U 1 1 5FB51C5E
P 5150 1950
F 0 "#PWR03" H 5150 1700 50  0001 C CNN
F 1 "GND" V 5155 1822 50  0000 R CNN
F 2 "" H 5150 1950 50  0001 C CNN
F 3 "" H 5150 1950 50  0001 C CNN
	1    5150 1950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FB521B4
P 5150 2500
F 0 "#PWR04" H 5150 2250 50  0001 C CNN
F 1 "GND" V 5155 2372 50  0000 R CNN
F 2 "" H 5150 2500 50  0001 C CNN
F 3 "" H 5150 2500 50  0001 C CNN
	1    5150 2500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FB5253B
P 5150 2950
F 0 "#PWR05" H 5150 2700 50  0001 C CNN
F 1 "GND" V 5155 2822 50  0000 R CNN
F 2 "" H 5150 2950 50  0001 C CNN
F 3 "" H 5150 2950 50  0001 C CNN
	1    5150 2950
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
