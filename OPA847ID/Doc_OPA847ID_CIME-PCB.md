# Amplificateur bande de base : OPA847ID

| Date          | Commentaire                                  |
|:-------------:|:-------------------------------------------- |
| 17 sept. 2021 | Création du fichier | 1er mesure S2P + Bruit |
| 20 sept. 2021 | ajout mesure point de compression + IP3      |
| 23 sept. 2021 | ajout mesure figure de bruit                 |

## Circuit

### Schéma
### Doc
[OPA847ID TI](https://www.ti.com/lit/ds/symlink/opa847.pdf?ts=1632215163207)

[OPA84ID Local](./doc/opa847.pdf)

### Photo
![Circuit imprimé](./photo/2021-09-17-OPA847ID-PCB_p.jpg)

### Consommation
| Tension | Courant |
| ------- | ------- |
| -5V     | 10 mA   |
| +5V     | 10 mA   |

### Puissance
maximal en entrée : -30 dBm

### Gain
| Fréquence | Gain  |
| --------- | ----- |
| 5 MHz     | 44 dB |
| 10 MHz    | 41 dB |
| 20 MHz    | 36 dB |

### Mesure Bruit
| Config             | Source bruit | Niveau   |
| ------------------ | ------------ | -------- |
| OPA847ID           | OFF          | -109 dBm |
| OPA847ID           | ON           | -103 dBm |
| ADL5611 + OPA847ID | OFF          | -111 dBm |
| ADL5611 + OPA847ID | ON           | -104 dBm |

Soit un delta ON/OFF pour l’OPA seul de 6 dB et pour la config ADL + OPA de 7 dB.

### Compression
|Frequence | Comp In   | Comp Out |
|----------|-----------|----------|
| 5 MHz    | -27 dBm   | 15,8 dBm |
| 10 MHz   | -24,5 dBm | 14,7 dBm |
| 20 MHz   | -18,9 dBm | 16,7 dBm |

### Intermodulation
|Frequence | IP3MO  |
|----------|--------|
| 5 MHz    | 41 dBm |
| 10 MHz   | 37 dBm |
| 20 MHz   | 30 dBm |

---

## Annexe Mesure
### Mesure VNA
![](mesure/2021-09-17-OPA847I9D/2021-09-17-OPA847ID.jpeg)

### Mesure Spectre
#### OPA847ID seul
##### Photo
![AOP PCB](./photo/2021-09-17-OPA-source-bruit_p.jpg)

##### AOP non alimenté
![DC off](mesure/2021-09-17-OPA847I9D/2021-09-17-OPA847ID-NO-DC.jpeg)

Niveau de bruit -128 dBm

##### AOP ON mais source de bruit OFF
![Source Off](mesure/2021-09-17-OPA847I9D/2021-09-17-OPA847ID-Source_OFF.jpeg)

Niveau de bruit -109 dBm

##### AOP ON et source de bruit ON
![](mesure/2021-09-17-OPA847I9D/2021-09-17-OPA847ID-Source_ON.png)

Niveau de bruit -103 dBm

#### ADL5611 + OPA847ID
##### Photo
![AOP](./photo/2021-09-17-ADL-OPA-source-bruit_p.jpg)

##### ADL + AOP ON mais source de bruit OFF
![](mesure/2021-09-17-OPA847I9D/2021-09-17-ADL5611-OPA847ID-Source_OFF.png)

Niveau de bruit -111 dBm

##### ADL + AOP ON et source de bruit ON
![](mesure/2021-09-17-OPA847I9D/2021-09-17-ADL5611-OPA847ID-Source_ON.png)

Niveau de bruit -104 dBm

#### Compression
##### 5MHz
![](./mesure/2021-09-20-OPA847ID_Compress_IP3/2021-09-20-OPA847ID-compress_5M_p.jpeg)

* Comp In -27 dBm
* Comp Out 15,8 dBm

##### 10MHz
![](./mesure/2021-09-20-OPA847ID_Compress_IP3/2021-09-20-OPA847ID-compress-10M_p.jpeg)

* Comp In -24,5 dBm
* Comp Out 14,7 dBm

##### 20MHz
![](./mesure/2021-09-20-OPA847ID_Compress_IP3/2021-09-20-OPA847ID-compress-20M_p.jpeg)

* Comp In : -18,9 dBm 
* Comp Out :16,7 dBm

#### Intermodulation
![](./mesure/2021-09-20-OPA847ID_Compress_IP3/2021-09-20-OPA847ID-IP3.jpeg)

#### Figure de bruit
![](./mesure/2021-09-21-OPA847ID-NF/2021-09-21_OPA847ID-NF.dat.jpg)
