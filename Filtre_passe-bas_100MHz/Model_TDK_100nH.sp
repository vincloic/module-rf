.subckt SIMID_0603_BASE1 A1 A2  PARAMS:  
+	ls11_a=11.0147n rs11_a=121m cp11_a=89.9051f rp11_a=9.1244k 
+	lwira1_a=300.6000p lwa1_a=2.3184n rwa1_a=351.0353m lwb1_a=4.8518n 
+	rwb1_a=8.6672 kw1a1_a=0.9999 
ls1_b	A1 31 	1.123861e-7
rs1_b	31 A2 	1.325
cp1_b	A1 A2 	8.61741e-14
rp1_b	A1 A2 	3.12615e4
lwira_b	900 0 	3.006000e-10
lwa_b	900 901 5.0818e-9
rwa_b	901 0 	1.1747
lwb_b	900 90b2  6.7049e-9
rwb_b	902 0 	7.4372  
kw1a_b	ls1_b lwira_b 0.9999
.ENDS